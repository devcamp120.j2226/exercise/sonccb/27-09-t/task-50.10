package com.devcamp.j02_javabasic.s10;

import java.util.ArrayList;

public class WarpperExample {
 /***
  * Autoboxing là cơ chế tự động chuyển đổi kiểu dữ liệu nguyên thủy sang object
  * của Wrapper class tương ứng.
  */
  public static void autoBoxing() {
   char ch = 'a';
   // Autoboxing- primitive to Character object conversion
   Character a = ch;
   // printing the values from object
   System.out.println(a);

   ArrayList<Integer> arrayList = new ArrayList<Integer>();
  // autoboxing became ArrayList stores only objects
  arrayList.add(25);
  // printing the values from object
  System.out.println(arrayList.get(0));
  }
  /**
   * Unboxing là cơ chế giúp chuyển đổi các object của wrapper class sang kiểu dữ
   * liệu nguyên thủy tương ứng
   */

  public static void unBoxing() {
   char ch = 'a';
   // unBoxing- primitive to Character object conversion
   Character a = ch;
   // printing the values from object
   System.out.println(a);

   ArrayList<Integer> arrayList = new ArrayList<Integer>();
  arrayList.add(24);
  // unboxing because get method return the integer object
  int num = arrayList.get(0); 
  // printing the values from object
  System.out.println(num);
  }
  public static void main(String[] args) {
   WarpperExample.autoBoxing();

   WarpperExample.unBoxing();
  }
}
